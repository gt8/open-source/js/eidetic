// @flow
class Aggregate {
  pendingEvents: Array<boolean>

  constructor() {
    this.pendingEvents = []
  }

  hasPendingEvents(): boolean {
    return this.pendingEvents.length !== 0
  }
}

module.exports = Aggregate
