// @flow
import Aggregate from './aggregate'

test('It has no pending events when a new Aggregate is created', () => {
  const user: Aggregate = new Aggregate()
  expect(user.hasPendingEvents()).toEqual(false)
})
