#
init:
	@yarn
	@./node_modules/.bin/flow-typed install jest@22.4.3

test:
	@yarn test

# Working with Docker
dshell:
	@docker-compose run --rm --entrypoint=bash js

dclean:
	@docker-compose down -v --rmi=local
